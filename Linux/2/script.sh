#!/bin/bash

if [[ "$1" == "-p" ]]; then
    `DISPLAY=:0 xterm -e "echo -------------------- Press Ctrl + C to turn Alarm off --------------------; mpg123 $2"`
else
    echo "****************************************"
    path=`readlink -f $0`" -p "
    regpath=".*\($path.*$\)"
    sc="0"
    TEMPFILE=$$.tmp
    `crontab -l | grep "$path" > "$TEMPFILE"`
    while read line; do
        let "sc = sc + 1"
        time=a"`expr "$line" : "\(\([^\s]\+\s\)\{4\}\)"`"
        i="0"
        sminute=""
        shour=""
        sday=""
        smonth=""
        for ss in ${time// / a}; do
            ti=${ss:1}
            if [[ ${#ti} -le 1 ]]; then
                ti="0$ti"
            fi
            case "$i" in
            "0" )
                sminute="$ti"
            ;;
            "1" )
                shour="$ti"
            ;;
            "2" )
                sday="$ti"
            ;;
            "3" )
                smonth="$ti"
            ;;
            esac
            let "i = i + 1"
        done
        echo "$sc | $shour:$sminute $sday.$smonth | ${line#*$path}"
    done < "$TEMPFILE"
    echo "****************************************
-a to add new Alarm
-e [NUM] to edit Alarm
-d [NUM] to delete Alarm
-q to quit
****************************************"

    function getNewAlarm () {
        echo "enter time in format [HH:MM]"
        read tm
        time="${tm:3:2} ${tm:0:2}"
        echo "enter date in format [DD.MM] (everyday by default)"
        read dt
        if [[ "$dt" == "" ]]; then
            time="$time * * *"
        else
            time="$time ${dt:0:2} ${dt:3:2} *"
        fi
        echo "enter /path/to/AlarmSound"
        read mp3file
        newAlarm="$time $path$mp3file"
    }

    function replaceAlarmString () {
        `crontab -l > "$TEMPFILE"`
        i="0"
        while read line; do
            if [[ `expr "$line" : "$regpath"` ]]; then
                let "i = i + 1"
                if [[ "$i" -ne "${f:3}" ]]; then
                    `echo "$line" >> "$NEWCRON"`
                else
                    `echo "$newAlarm" >> "$NEWCRON"`
                fi
            else
                `echo "$line" >> "$NEWCRON"`
            fi
        done < "$TEMPFILE"
    }

    read f
    NEWCRON=$$.tmpcron
    case "${f:0:2}" in
    "-a" )
        newAlarm=""
        getNewAlarm
        `crontab -l > $NEWCRON`
        `echo "$newAlarm" >> "$NEWCRON"`
        `crontab "$NEWCRON"`
    ;;
    "-e" )
        newAlarm=""
        getNewAlarm
        replaceAlarmString
        `crontab "$NEWCRON"`
    ;;
    "-d" )
        newAlarm=""
        replaceAlarmString
        `crontab "$NEWCRON"`
    ;;
    "-q" )
    ;;
    esac
    `rm -f $TEMPFILE $NEWCRON`
fi

exit 0
