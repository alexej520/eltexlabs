#!/bin/bash

echo "----------------------------------------
Select folder, press \"enter\" for `pwd`"
read folder_name
if [[ "$folder_name" == "" ]]; then
    folder_name=`pwd`
fi

echo "----------------------------------------
Select number of subfolders in folder, press \"enter\" for 5"
read sub_number
if [[ "$sub_number" == "" ]]; then
    sub_number=5
fi

echo "Select namepattern of subfolders"
read sub_np

echo "----------------------------------------
Select number of subsubfolders in subfolder, press \"enter\" for 10"
read subsub_number
if [[ "$subsub_number" == "" ]]; then
    subsub_number=10
fi

echo "Select namepattern of subsubfolders"
read subsub_np

echo "----------------------------------------
Select number of files in subsubfolder, press \"enter\" for 20"
read f_number
if [[ "$f_number" == "" ]]; then
    f_number=20
fi

echo "Select namepattern of files"
read f_np

mkdir -p "$folder_name"
for (( subc=1; subc<=sub_number; subc++ )); do
	mkdir -p "$folder_name/$sub_np$subc"
	for (( subsubc=1; subsubc<=subsub_number; subsubc++ )); do
		mkdir -p "$folder_name/$sub_np$subc/$subsub_np$subsubc"
		for (( fc=1; fc<=f_number; fc++ )); do
			touch "$folder_name/$sub_np$subc/$subsub_np$subsubc/$f_np$fc"
		done
	done
done

exit 0
