#!/bin/bash

if [[ "$1" == "--help" ]]; then
	echo "Synopsis
`basename $0` process-name first-date [ last-date ]"
elif [[ $# -ge 2 && $# -le 3 ]]; then
	pName=$1
	fDate=`date -d "$2" +%s`
	if [[ "$3" == "" ]]; then
		lDate=`date +%s`
	else
		lDate=`date -d "$3" +%s`
	fi

	# $1 File
	function compareDate () {
	    while read line; do
	        Date=`expr "$line" : "\(\([^[:space:]]\+\s\)\{3\}\)"`
	        Date=`date -d "$Date" +%s`
	        if [[ "$Date" -le "$lDate" && "$Date" -gt "$fDate" ]]; then
	            echo "$line"
	        fi
	    done < "$1"
	    return 0
	}

	echo "****************************************"
	TMP0=$$0.tmp
	TMP1=$$1.tmp

	# in log/pName
	`find /var/log/*$pName* -type f > "$TMP0" 2>/dev/null`
	while read f; do
	    `grep "$pName" "$f" > "$TMP1"`
	    compareDate "$TMP1"
	done < "$TMP0"
	echo "****************************************"
	if [[ `wc -l < $TMP0` -gt 0 ]]; then
		`rm -f $TMP0 $TMP1`
		exit 0
	fi

	# in log/pNane/*
	`find /var/log/ -maxdepth 1 -type d -name *$pName* > "$TMP0"`
	while read d; do
	    `find $d -type f > "$TMP1"`
	    while read f; do
	        compareDate "$f"
	    done < "$TMP1"
	done < "$TMP0"
	echo "****************************************"
	if [[ `wc -l < $TMP0` -gt 0 ]]; then
		`rm -f $TMP0 $TMP1`
		exit 0
	fi

	# in log/syslog
	`grep "$pName" "/var/log/syslog" > "$TMP1"`
	compareDate
	echo "****************************************"
	`rm -f $TMP0 $TMP1`
else
	echo "Invalid parametrs, Use --help to get Manual"
fi

exit 0
