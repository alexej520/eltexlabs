#!/bin/bash

TMP=/tmp/$$.tmp

# $1 command7
# $2 check
# $3 title
function info (){
    if [[ $? == 0 || "$2" == "-n" ]]; then
        MSG=$(git $1 2>&1)
        MSGLEN=$(wc -l <<< "$MSG")
        if [[ $MSGLEN -le 18 ]]; then
            whiptail --msgbox "${MSG:-"Successfully"}" --title "${3:-"Info"}" 24 80 3>&1 1>&2 2>&3
        else
            whiptail --scrolltext --msgbox "${MSG:-"Successfully"}" --title "${3:-"Info"}" 24 80 3>&1 1>&2 2>&3
        fi
    fi
    return 0    
}

# $1 command
# $2 title
# $3 inputbox
function manual (){
	MSGLEN=$(wc -l <<< "$3")
	if [[ $MSGLEN -le 18 ]]; then
		USERINPUT=$(whiptail --title "$2" --inputbox "$3" 24 80 3>&1 1>&2 2>&3)
	else
		USERINPUT=$(whiptail --scrolltext --title "$2" --inputbox "$3" 24 80 3>&1 1>&2 2>&3)
	fi
    info "$1 $USERINPUT"
}

# $1 command
# $2 title
# $3 hint
# $4 default OFF
function status-menu (){
    STATUS=()
    git status -s >$TMP
    while read status name; do
        STATUS+=( "$name" "$status" "${4:-"OFF"}" )
    done < $TMP
    list=$(whiptail --separate-output --title "$2" --checklist "$3" 24 80 18 "${STATUS[@]}" 3>&1 1>&2 2>&3)
    if [[ "${list[@]}" != "" ]]; then
        info "$1 ${list[@]}"
    fi
    return 0
}

# $1 command
# $2 title
function status-manual (){
    STATUS=""
    git status -s >$TMP
    while read status; do
        STATUS+="$status"$'\n'
    done < $TMP
    manual "$1" "$2" "$STATUS"
    return 0
}

# $1 command
# $2 title
# $3 hint
function remote-menu (){
    REMOTES=()
    git remote >$TMP
    while read name; do
        REMOTES+=( "$name" "" "OFF" )
    done < $TMP
    list=$(whiptail --separate-output --title "$2" --radiolist "$3" 24 80 18 "${REMOTES[@]}" 3>&1 1>&2 2>&3)
    if [[ "$list" != "" ]]; then
        info "remote $1 $list"
    fi
    return 0
}

# $1 command
# $2 title
function remote-manual (){
    REMOTES=""
    git remote >$TMP
    while read remote; do
        REMOTES+="$remote"$'\n'
    done < $TMP
    manual "remote $1" "$2" "$REMOTES"
    return 0
}

# no params
function remote-fetch-push (){
	curr=$(git branch|grep \*)
	REMOTES=()
    git remote -v >$TMP
    while read name URL fetch_push; do
        fetch_push=${fetch_push//(}
        fetch_push=${fetch_push//)}
        REMOTES+=( "$fetch_push $name" "$URL" "OFF" )
    done < $TMP
    list=$(whiptail --separate-output --title "Remote Fetch/Push" --radiolist "Select Option" 24 80 18 "${REMOTES[@]}" 3>&1 1>&2 2>&3)
    git $list ${curr:2}
    return 0
}

# $1 command
# $2 title
# $3 hint
function branch-menu (){
	BRANCHES=()
    git branch >$TMP
    while read status branch; do
    	if [[ "$branch" != "" ]]; then
    		BRANCHES+=( "$branch" "" "ON" )
    	else
    		BRANCHES+=( "$status" "" "OFF" )
    	fi
    done < $TMP
    list=$(whiptail --separate-output --title "$2" --radiolist "$3" 24 80 18 "${BRANCHES[@]}" 3>&1 1>&2 2>&3)
    if [[ "$list" != "" ]]; then
        info "$1 $list"
    fi
    return 0
}

OPTION="1"

while [[ 1 ]]; do
    OPTION=$(whiptail --default-item "$OPTION" --nocancel --title "Main menu" --menu "Select option" 24 80 16 \
"1" "Initialize" \
"2" "Clone" \
"3" "Status" \
"4" "Add New Files" \
"5" "Add New Files Manually" \
"6" "Commit" \
"7" "Amend Commit" \
"8" "Divergent History" \
"9" "Staged and Unstaged Changes" \
"10" "Pull" \
"11" "Commit All Changed Files" \
"12" "Remove Files (Manually)" \
"13" "Move Files (Manually)" \
"14" "Custom Log" \
"15" "Unmodifying a Modified Files" \
"16" "Unstaging a Staged Files" \
"17" "Add Remote Repository" \
"18" "Remove from the Index only (Manually)" \
"19" "Show Remote" \
"20" "Config Global" \
"21" "Fetch/Push" \
"22" "Rename Remote" \
"23" "Remove Remote" \
"24" "Show Info about Remote" \
"25" "Edit .gitignore" \
"26" "New Branch" \
"27" "Checkout" \
"28" "Merge into Current Branch" \
"29" "Delete Branch" \
"30" "MergeTool" \
"31" "Exit" 3>&1 1>&2 2>&3)
    case "$OPTION" in
    "1" )
        info "init" "-n"
        ;;
    "2" )
        manual "clone" "Clone Repository" "Git URL"
        ;; 
    "3" )
        info "status" "-n" "Git Status"
        ;;
    "4" )
        status-menu "add" "Add to Git" "Select Files" "ON"
        ;;
    "5" )
        status-manual "add" "Add to Git"
        ;;
    "6" )
        git commit
        ;;
    "7" )
        git commit --amend
        ;;
    "8" )
        info "log --oneline --decorate --graph --all" "-n" "Git Divergent History"
        ;;
    "9" )
        info "diff" "-n" "Git Diff"
        ;;
    "10" )
        info "pull" "-n"
        ;;
    "11" )
        git commit -a
        ;;
    "12" )
        manual "rm" "Remove from Git and Working Directory"
        ;;
    "13" )
        status-manual "mv" "Move Files"
        ;;
    "14" )
        manual "log" "Git Custom Log" "Enter log params"
        ;;
    "15" )
        status-menu "checkout --" "Unmodifying a Modified Files" "Select Files"
        ;;
    "16" )
        status-menu "reset HEAD" "Unstaging a Staged Files" "Select Files"
        ;;
    "17" )
        manual "remote add" "Add Remote Repository" "Git URL"
        ;;
    "18" )
        status-manual "rm --cached" "Remove from the Index only"
        ;;
    "19" )
        info "remote -v" "-n" "Remotes"
        ;;
    "20" )
        manual "config --global" "Git Global Config" "Key [Value]"$'\n'\
"user.name"$'\n'\
"user.email"$'\n'\
"core.editor"$'\n'\
"credential.helper cache"$'\n'\
"push.default matching|simple"$'\n'\
"--list"
        ;;
    "21" )
        remote-fetch-push
        ;;
    "22" )
        remote-manual "rename" "Rename Remote"
        ;;
    "23" ) 
        remote-menu "rm" "Remove Remote" "Select Remote"
        ;;
    "24" )
        remote-menu "show" "Show Info about Remote" "Select Remote"
        ;;
    "25" )
        $(git config core.editor) .gitignore
        ;;
    "26" )
		manual "branch" "Creating a New Branch" "Name"
		;;
	"27" )
		branch-menu "checkout" "Checkout" "Current branch by default"
		;;
	"28" )
		branch-menu "merge" "Merge into Current Branch" "Current branch by default"
		;;
	"29" )
		branch-menu "branch -d" "Delete Branch" "Current branch by default"
		;;
	"30" )
		git mergetool
		;;
	"31" )
		break
		;;
    esac
done

`rm -f "$TMP"`
exit 0
