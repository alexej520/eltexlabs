#!/bin/bash

if [[ "$1" == "-check" ]]; then
    res=`ps axo command | grep "${*//$1/}" | wc -l`
    if [[ "$res" -le 3 ]]; then
        command ${*//$1/}
    fi    
else
    TEMPFILE=$$.tmp
    path=`readlink -f $0`
    `crontab -l | grep "$path" > "$TEMPFILE"`
    sc="0"
    echo "****************************************"
    while read line; do
        let "sc = sc + 1"
        echo "$sc | ${line//$path -check/|}"
    done < "$TEMPFILE"
    echo "****************************************
-a add new entry
-e [NUM] edit NUM entry
-d [NUM] delete NUM entry
-q quit
'****************************************"

    function getNewEntry () {
        echo "enter process name (command)"
        read newEntry
        echo "enter frequency of checks like in cron [m h dom mon dow]"
        read nEtime
        newEntry="$nEtime $path -check $newEntry"
    }

    function replaceEntry () {
        `crontab -l > "$TEMPFILE"`
        i="0"
        while read line; do
            if [[ `expr "$line" : ".*\($path.*$\)"` ]]; then
                let "i = i + 1"
                if [[ "$i" -ne "${cm:3}" ]]; then
                    `echo "$line" >> "$NEWCRON"`
                else
                    `echo "$newEntry" >> "$NEWCRON"`
                fi
            else
                `echo "$line" >> "$NEWCRON"`
            fi
        done < "$TEMPFILE"
    }

    read cm
    NEWCRON=$$.tmpcron
    case "${cm:0:2}" in
    "-a" )
        `crontab -l > "$NEWCRON"`
        newEntry=""
        getNewEntry
        `echo "$newEntry" >> "$NEWCRON"`
        `crontab "$NEWCRON"`
    ;;
    "-e" )
        newEntry=""
        getNewEntry
        replaceEntry
        `crontab "$NEWCRON"`
    ;;
    "-d" )
        newEntry=""
        replaceEntry
        `crontab "$NEWCRON"`
    ;;
    "-q" )
    ;;
    esac
    `rm -f $TEMPFILE $NEWCRON`
fi

exit 0
