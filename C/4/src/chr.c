// ЗАДАНИЕ:
// В лабораторной работе требуется написать две программы для обработки текстовых файлов. Одна из них выполняет построчную, другая посимвольную обработку:
//  1. Написать программу, обрабатывающую текстовый файл и  записывающую обработанные данные в файл с таким же именем, но   с другим типом (табл. 3).

//  2.Написать программу, выполняющую посимвольную обработку    текстового файла (табл. 4).
// 3
// Заменить цифры на пробелы
// Параметры командной строки:
//     1. Имя входного файла
//     2. Количество замен

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>  // isdigit()

char *repl_ext(char *fn, const char *ext) {
    char *last_point = strrchr(fn, '.');
    if (last_point == NULL) last_point = strrchr(fn, '\0');
    char *nfn = calloc(last_point - fn + strlen(ext) + 1, sizeof(char));
    strncpy(nfn, fn, last_point - fn);
    strcat(nfn, ext);
    return nfn;
}

FILE *open_file(char *name, char *mode){
    FILE *f = fopen(name, mode);
    if (f == NULL) {
        printf("cannot open file: %s\n", name);
        exit(-1);
    }
    return f;
}

int main(int argc, char *argv[])
{
    if (argc != 3) {
        printf("wrong arguments, use: %s filename replace_num\n", argv[0]);
        return 0;
    }

    int nrepl = atoi(argv[2]);

    char *in_n = argv[1];
    FILE *in_f = open_file(in_n, "r");

    char *out_n = repl_ext(in_n, ".out");
    FILE *out_f = open_file(out_n, "w");
    free(out_n);

    int i = 0; char ch;
    while ((ch = fgetc(in_f)) != EOF)
        if (i < nrepl && isdigit(ch)) {
            fputc(' ', out_f);
            ++i;
        }
        else {
            fputc(ch, out_f);
        }

    int cs = fclose(in_f);
    cs = fclose(out_f) && cs;
    if (cs){
        printf("close file error\n");
        exit(-2);
    }

    return 0;
}
