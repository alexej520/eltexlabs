#include <parent.h>

void draw() {
    char line[TERMINAL_WIDTH + 1];
    char *linecur;
    char *linemax = &line[TERMINAL_WIDTH - PLAYER_STATUS_WIDTH];
    gotoxy(0, 0);
    for (int i = 0; i < HEIGHT; ++i) {
        for (int j = 0; j < WIDTH; ++j)
            printf("+---");
        printf("+%-*s\n", RIGHT_BORDER, "");
        for (int j = 0; j < WIDTH; ++j)
            printf("|%2d ", map[i][j]);
        printf("|%-*s\n", RIGHT_BORDER, "");
    }
    for (int j = 0; j < WIDTH; ++j)
        printf("+---");
    printf("+%-*s\n", RIGHT_BORDER, "");
    //qsort(players, MAX_PLAYERS, sizeof(struct player), compare_players);
    for (int i = 0; i < MAX_PLAYERS; ++i) {
        struct player *p = &players[i];
        gotoxy((p->x + 1) * CELL_WIDTH - 2, (p->y + 1) * CELL_HEIGHT);
        if (p->name != 0)
            p->hp > 0 ? printf("(%C)", p->name) : printf("/%C\\", p->name);
    }

    gotoxy(1, LEGEND_Y);
    linecur = &line[0];
    for (int i = 0; i < MAX_PLAYERS && linecur < linemax; ++i) {
        struct player *p = &players[i];
        char name = p->name != 0 ? p->name : NLOAD_NAME;
        linecur += p->hp > 0 ? sprintf(linecur, "(%1C)%2d  ", name, p->hp) : sprintf(linecur, "/%1C\\%2d  ", name, p->hp);
    }
    printf("%-*.*s", TERMINAL_WIDTH, TERMINAL_WIDTH, line);

    gotoxy(1, PID_Y);
    linecur = &line[0];
    for (int i = 0; i < MAX_PLAYERS && linecur < linemax; ++i)
        linecur += sprintf(linecur, "%-5d  ", pids[i]);
    printf("%-*.*s", TERMINAL_WIDTH, TERMINAL_WIDTH, line);

    gotoxy(1, STATUS_Y);
    printf("%*.*s", TERMINAL_WIDTH, TERMINAL_WIDTH, "");

    gotoxy(1, STATUS_Y);
    fflush(stdout);
}

void parent_init() {
    srand(getpid());
    init_map(-3, 3);
    init_names();
}

void parent_process() {
    if (close(fd[1])) {
        printf("error close write pipe \n");
        exit(EXIT_FAILURE);
    }
    int completed_childs = 0;
    int pid_status = 0;
    replace_mode();
    clean();
    for (;;) {
        int id;
        while (id = read_data()) {
            calc_player_hp(id);
            if (players[id].hp == 0)
                kill(pids[id], 9);
        }
        draw();
        if (waitpid(0, &pid_status, WNOHANG))
            completed_childs++;

        if (completed_childs == MAX_PLAYERS) {
            if (close(fd[0]))
                exit(EXIT_FAILURE);
            exit(EXIT_SUCCESS);
        }
        usleep(FRAME_DELAY);
    }
}
