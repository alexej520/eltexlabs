#include <core.h>
#include <child.h>

void child_process(int id) {
    srand(getpid());
    double lifetime = 0;
    struct player *p = NULL;
    for (;;)
    {
        if ((fd = open(fn, O_RDWR)) > 0) {
            if (lockf(fd, F_TLOCK, 0)) {
                ;
            }
            else {
                read_data();
                if (!p) p = create_player(id);
                step_player(p);
                if (lifetime >= PLAYER_MAX_LIFETIME) p->hp = 0;
                write_data();
                lockf(fd, F_ULOCK, 0);
            }
            if (close(fd) < 0) exit(EXIT_FAILURE);
        }
        if (p && p->hp <= 0) exit(EXIT_SUCCESS);
        usleep(PLAYER_DELAY);
        lifetime += PLAYER_DELAY * 1e-6;
    }
}
