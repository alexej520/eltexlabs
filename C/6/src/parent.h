#include <core.h>

#define clean() printf("\033[H\033[J")
#define gotoxy(x,y) printf("\033[%d;%dH", (y), (x))
#define replace_mode() printf("\0334l")

#define RIGHT_BORDER (TERMINAL_WIDTH - 1) % CELL_WIDTH
#define PLAYER_STATUS_WIDTH 7
#define LEGEND_Y TERMINAL_HEIGHT - 2
#define PID_Y TERMINAL_HEIGHT - 1
#define STATUS_Y TERMINAL_HEIGHT
#define FRAME_DELAY 50000  // us

extern void parent_process();

extern void parent_init();
