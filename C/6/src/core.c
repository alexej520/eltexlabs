#include <core.h>

int map[HEIGHT][WIDTH];
struct player players[MAX_PLAYERS];
char fn[L_tmpnam];
int fd;
char names[MAX_PLAYERS];
int pids[MAX_PLAYERS];

void clamp(int *x, int min, int max) {
    *x = *x < min ? min : *x >= max ? max - 1 : *x;
}

void read_data() {
    lseek(fd, 0, SEEK_SET);
    read(fd, map, sizeof(map));
    read(fd, players, sizeof(struct player) * MAX_PLAYERS);
}

void write_data() {
    lseek(fd, 0, SEEK_SET);
    write(fd, map, sizeof(map));
    write(fd, players, sizeof(struct player) * MAX_PLAYERS);
}

struct player *create_player(int id) {
    struct player *p = &players[id];
    p->name = names[id];
    p->hp = START_HP;
    p->id = id;
    p->x = rand() % WIDTH;
    p->y = rand() % HEIGHT;
    return p;
}

void step_player(struct player * p)
{
    if (p->hp <= 0) return;
    int d = rand_range(0, 4);
    switch (d) {
    case 0: p->y--; break;  // up
    case 1: p->x++; break;  // right
    case 2: p->y++; break;  // down
    case 3: p->x--; break;  // left
    }
    clamp(&p->x, 0, WIDTH);
    clamp(&p->y, 0, HEIGHT);
    int *cell = &map[p->y][p->x];
    p->hp += *cell;
    *cell = 0;
    for (int i = 0; i < MAX_PLAYERS; ++i) {
        if (i == p->id) continue;
        struct player *p2 = &players[i];
        if (p2->hp > 0 && p->x == p2->x && p->y == p2->y) {
            int min_hp = min(p->hp, p2->hp);
            p->hp -= min_hp;
            p2->hp -= min_hp;
        }
    }
    p->hp = max(p->hp, 0);
}

void battles() {
    for (int i = 0; i < MAX_PLAYERS; i++) {
        struct player *p1 = &players[i];
        for (int j = i + 1; j < MAX_PLAYERS; ++j)
        {
            struct player *p2 = &players[j];
            if (p1->x == p2->x && p1->y == p2->y) {
                int min_hp = min(p1->hp, p2->hp);
                p1->hp -= min_hp;
                p2->hp -= min_hp;
            }
        }
    }
}

int compare_players(const void *o1, const void *o2) {
    struct player *p1 = (struct player *) o1;
    struct player *p2 = (struct player *) o2;
    return p1->y < p2->y ? -1 : p1->y > p2->y ? 1 :
           p1->x < p2->x ? -1 : p1->x > p2->x ? 1 : 0;
}

void init_names(){
    int i = 0;
    for (; i < MAX_PLAYERS; ++i)
        if (NAMES[i])
            names[i] = NAMES[i];
        else
            break;
    for (; i < MAX_PLAYERS; ++i)
        names[i] = DEFAULT_NAME;
}

void init_map(int minhp, int maxhp) {
    for (int i = 0; i < HEIGHT; ++i)
        for (int j = 0; j < WIDTH; ++j)
            map[i][j] = rand_range(minhp, maxhp);
}
