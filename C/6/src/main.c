#include <child.h>
#include <parent.h>

// +--+--+---+---+---+
// |-1| 0|-1 |[$]|(#)|
// +--+--+---+---+---+



int main(int argc, char const * argv[])
{
    parent_init();

    for (int p_id = 0; p_id < MAX_PLAYERS; p_id++) {
        pid_t pid = fork();
        // error
        if (pid == -1) {
            printf("error create process\n");
            exit(EXIT_FAILURE);
        }
        // player
        else if (pid == 0) {
            child_process(p_id);
        }
        // parent process loop
        else {
            pids[p_id] = pid;
        }
    }
    parent_process();
}
