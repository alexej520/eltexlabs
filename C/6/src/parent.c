#include <parent.h>

void draw() {
    char line[TERMINAL_WIDTH + 1];
    char *linecur;
    char *linemax = &line[TERMINAL_WIDTH - PLAYER_STATUS_WIDTH];
    gotoxy(0, 0);
    for (int i = 0; i < HEIGHT; ++i) {
        for (int j = 0; j < WIDTH; ++j)
            printf("+---");
        printf("+%-*s\n", RIGHT_BORDER, "");
        for (int j = 0; j < WIDTH; ++j)
            printf("|%2d ", map[i][j]);
        printf("|%-*s\n", RIGHT_BORDER, "");
    }
    for (int j = 0; j < WIDTH; ++j)
        printf("+---");
    printf("+%-*s\n", RIGHT_BORDER, "");
    //qsort(players, MAX_PLAYERS, sizeof(struct player), compare_players);
    for (int i = 0; i < MAX_PLAYERS; ++i) {
        struct player *p = &players[i];
        gotoxy((p->x + 1) * CELL_WIDTH - 2, (p->y + 1) * CELL_HEIGHT);
        if (p->name != 0)
            p->hp > 0 ? printf("(%C)", p->name) : printf("/%C\\", p->name);
    }

    gotoxy(1, LEGEND_Y);
    linecur = &line[0];
    for (int i = 0; i < MAX_PLAYERS && linecur < linemax; ++i) {
        struct player *p = &players[i];
        char name = p->name != 0 ? p->name : NLOAD_NAME;
        linecur += p->hp > 0 ? sprintf(linecur, "(%1C)%2d  ", name, p->hp) : sprintf(linecur, "/%1C\\%2d  ", name, p->hp);
    }
    printf("%-*.*s", TERMINAL_WIDTH, TERMINAL_WIDTH, line);

    gotoxy(1, PID_Y);
    linecur = &line[0];
    for (int i = 0; i < MAX_PLAYERS && linecur < linemax; ++i)
        linecur += sprintf(linecur, "%-5d  ", pids[i]);
    printf("%-*.*s", TERMINAL_WIDTH, TERMINAL_WIDTH, line);

    gotoxy(1, STATUS_Y);
    printf("%*.*s", TERMINAL_WIDTH, TERMINAL_WIDTH, "");

    gotoxy(1, STATUS_Y);
    fflush(stdout);
}

void parent_init(){
    srand(getpid());
    init_map(-3, 3);
    init_names();
    tmpnam(fn);
    // strcpy(fn, "tmp");
    if ((fd = open(fn, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR)) < 0) {
        printf("error open temp file : %s\n", fn);
        exit(EXIT_FAILURE);
    }
    write_data();
    if (close(fd) < 0) {
        printf("error close temp file : %s\n", fn);
        exit(EXIT_FAILURE);
    }
}

void parent_process() {
    int completed_childs = 0;
    int pid_status = 0;
    replace_mode();
    clean();
    struct flock fl = {
        .l_whence = SEEK_SET,
        .l_start = 0,
        .l_len = 0
    };
    for (;;) {
        if ((fd = open(fn, O_RDONLY)) > 0) {
            if (fl.l_type = F_RDLCK, fcntl(fd, F_SETLK, &fl)) {
                ;
            }
            else {
                read_data();
                //battles();
                draw();
                //write_data();
                fl.l_type = F_UNLCK, fcntl(fd, F_SETLK, &fl);
            }
            if (close(fd) < 0) exit(EXIT_FAILURE);
        }
        if (waitpid(0, &pid_status, WNOHANG))
            completed_childs++;
        if (completed_childs == MAX_PLAYERS)
            exit(EXIT_SUCCESS);
        usleep(FRAME_DELAY);
    }
}
