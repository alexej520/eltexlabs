//ЗАДАНИЕ:
//1. 
//Написать программу, работающую с базой данных в виде массива структур и выполняющую последовательный ввод данных в массив и последующую распечатку его содержимого. 
//Состав структуры приведен в варианте.
//Типы данных выбрать самостоятельно.
//При написании программы следует использовать статические массивы структур или указателей на структуру.
//Размерности массивов – 3–4. 
//Ввод данных выполнить с помощью функций scanf().

//2. 
//Модифицировать программу, используя массив указателей на структуру и динамическое выделение памяти. 
//Выполнить сортировку массива с помощью функции qsort. 
//Способ сортировки массива приведен в варианте.
//Для динамического выделения памяти используйте функцию malloc().
//Для определения размера структуры в байтах удобно использовать операцию sizeof(), возвращающую целую константу: 
    //struct ELEM *sp; 
    //sp = malloc(sizeof(struct ELEM));
//7
//Фамилия 
//Группа 
//Номер в списке 
//Стипендия 
//Расположить записи в порядке возрастания номера в списке 


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mcheck.h>

struct info {
    char name[21];
    char group[7];
    unsigned char n;
    unsigned int st;
};

void print_info(struct info *i){
    printf("%u\t%s\t%s\t%u\n", i->n, i->name, i->group, i->st);
}

int comp_info(const void *v1, const void *v2){
    struct info *i1 = *(struct info **) v1;
    struct info *i2 = *(struct info **) v2;
    return i1->n > i2->n ? 1 : i1->n < i2->n ? -1 : 0;
}

int i = 0;

int main(int argc, char **argv)
{
    unsigned int max = 0;
    scanf("%u", &max);

    struct info **data = malloc(sizeof(struct info *) * max);
    struct info *e;
    int scanV;
    
    do{
        e = malloc(sizeof(struct info));
        data[i++] = e;
        scanV = scanf("%s %s %hhu %u", e->name, e->group, &e->n, &e->st);
    }
    while(scanV != EOF);

    qsort(data, i, sizeof(struct info *), comp_info);

    for(int j = 0; j < i; j++){
            print_info(data[j]);
            free(data[j]);
        }
    free(data);

    return 0;
}
