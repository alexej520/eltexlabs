#include <core.h>
#include <child.h>

void child_process(int id) {
    if (close(fd[0])){
        printf("error close read pipe \n");
        exit(EXIT_FAILURE);
    }
    srand(getpid());
    double lifetime = 0;
    for (;;)
    {
        step_player(id);
        write_data(id);
        usleep(PLAYER_DELAY);
        lifetime += PLAYER_DELAY * 1e-6;
        if (lifetime >= PLAYER_MAX_LIFETIME){
            if (close(fd[1]))
                exit(EXIT_FAILURE);
            exit(EXIT_SUCCESS);
        }
    }
}
