#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define rand_range(min,max) (min) + rand() % ((max) - (min))
#define itoc(x) '0' + (x)
#define min(a,b) (a) < (b) ? (a) : (b)
#define max(a,b) (a) > (b) ? (a) : (b)

#define CELL_WIDTH 4
#define CELL_HEIGHT 2
#define TERMINAL_WIDTH 80
#define TERMINAL_HEIGHT 24
#define WIDTH (TERMINAL_WIDTH - 1) / CELL_WIDTH
#define HEIGHT (TERMINAL_HEIGHT - 4) / CELL_HEIGHT
#define START_HP 1
#define MAX_PLAYERS 8
#define NLOAD_NAME ' '
#define DEFAULT_NAME '?'
#define NAMES "*@#$%&^!"
#define PLAYER_DELAY 500000  // us
#define PLAYER_MAX_LIFETIME 20  // s

struct player {
    char name;
    int hp;
    int x;
    int y;
    int id;
};

extern int map[HEIGHT][WIDTH];
extern struct player players[MAX_PLAYERS];
extern char fn[L_tmpnam];
extern int fd[2];
extern char names[MAX_PLAYERS];
extern pid_t pids[MAX_PLAYERS];

extern void clamp(int *x, int min, int max);

extern int read_data();

extern void write_data();

extern struct player *create_player(int id);

extern void step_player(int id);

extern void calc_player_hp(int id);

extern void init_names();

extern void battles();

extern int compare_players(const void *o1, const void *o2);

extern void init_map(int minhp, int maxhp);
