//ЗАДАНИЕ:
//Написать программу сортировки массива строк по вариантам.
//Строки вводить с клавиатуры.
//Сначала пользователь вводит кол-во строк потом сами строки. 
//Для тестирования использовать перенаправление ввода-вывода ($./myprog < test.txt). 
//Память для строк выделять динамически с помощью функций malloc или calloc.
//Ввод данных, сортировку и вывод результатов оформить в виде функций.
//Сортировку делать с помощью qsort если возможно, если нет то писать свой вариант сортировки.
//Входные и выходные параметры функции сортировки указаны в варианте. 
//Входные и выходные параметры функций для ввода-вывода:
//10 
//Расположить строки по убыванию длины первого слова
//Входные параметры:
//1. Массив 
//2. Размерность массива
//Выходные параметры:
//1. Минимальная длина слова
//2. Количество слов в первой строке 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXLEN 80

int inp_str(char* string, int maxlen);
void out_str(char* string, int length, int number);

char **str_array;
int str_n;
char tmp[MAXLEN];

int main(int argc, char **argv)
{
    scanf("%d", &str_n);
    str_array = malloc(sizeof(char **) * str_n);
    for(int i = 0; i < str_n; i++){
        scanf("%s", tmp);
        inp_str(tmp, MAXLEN);
    }
    return 0;
}

// string – выводимая строка 
// length – длина строки 
// number – номер строки 
void out_str(char* string, int length, int number){
    printf("%d\t%d\t%s\t\n", number, length, string);
}

// string – введенная строка 
// maxlen – максимально возможная длина строки (размерность массива string)
// return – длина строки 
int inp_str(char* string, int maxlen){
    int len = strlen(string);
    if (len > maxlen){
        len = maxlen - 1;
    }
    else{
        str_array[]
    }
    return len;
}

void srt_str(char** str_array, int str_len, int str_n){
    //qsort(str_array,)
}
