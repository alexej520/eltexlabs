#include <stdlib.h>
#include <stdio.h>

extern double pow3(double n);
extern double pow4(double n);

int main(int argc, char const *argv[])
{
    for(int i = 1; i < argc; i++){
        double d = atof(argv[i]);
        printf("%.4g\t%.4g\t%.4g\n", d, pow3(d), pow4(d));
    }
    return 0;
}
