#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>

int main(int argc, char const *argv[])
{
    void *ext_lib = dlopen("/home/alexej520/Documents/EltexLabs/C/5/libpowdyn.so", RTLD_LAZY);
    if (!ext_lib) exit(-1);

    double (*pow3)(double x) = dlsym(ext_lib, "pow3");
    double (*pow4)(double x) = dlsym(ext_lib, "pow4");

    for(int i = 1; i < argc; i++){
        double d = atof(argv[i]);
        printf("%.4g\t%.4g\t%.4g\n", d, pow3(d), pow4(d));
    }
    return 0;
}
